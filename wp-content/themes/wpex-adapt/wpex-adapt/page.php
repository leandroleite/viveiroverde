<?php
/**
 * @package WordPress
 * @subpackage Adapt Theme
 */
?>
<?php get_header(); ?>

<?php while (have_posts()) : the_post(); ?>

    <header id="page-heading">
        <h1><?php the_title(); ?></h1>		
    </header><!-- /page-heading -->
    <style>
        #page-featured-img-2 {
            display: inline-block;
            width: 30%;
            float: left;
            margin-right: 10px;
        }
    </style>
    <?php if (has_post_thumbnail()) : ?>
        <div id="page-featured-img-2" class="container clr">
            <?php $img = get_the_post_thumbnail(null, $size, $attr); ?>
            <?php preg_match_all('/http\:\/\/([^\"])+/', $img, $match); ?>
            <img src="<?= $match[0][0] ?>">
        </div><!-- #page-featured-img -->
    <?php endif; ?>

    <article class="post clearfix">
        <div class="entry clearfix">	
            <?php the_content(); ?> 
        </div><!-- /entry -->    
        <?php comments_template(); ?>
    </article><!-- /post -->

<?php endwhile; ?>

<?php //get_sidebar(); ?>
<?php get_footer(); ?>