<?php
/**
 * Template Name: Parent Page
 *
 * @package WordPress
 * @subpackage Custom Theme
 */
?>

<?php get_header(); ?>

<?php $children = get_children(array('post_type' => 'page', 'post_parent' => get_the_ID(), 'post_status' => 'publish')) ?>
<?php while (have_posts()) : the_post(); ?>
    <?php
    // Load isotope scripts
    wp_enqueue_script('isotope', WPEX_JS_DIR . '/isotope.js');
    wp_enqueue_script('isotope_init', WPEX_JS_DIR . '/isotope_init.js');
    ?>

    <header id="page-heading" class="clearfix">
        <h1><?php the_title(); ?></h1>	
        <?php $terms = get_terms('portfolio_category'); ?>
        <?php if ($terms[0]) { ?>
            <ul id="portfolio-cats" class="filter clearfix">
                <li><a href="#" class="active" data-filter="*"><span><?php _e('All', 'wpex'); ?></span></a></li>
                <?php foreach ($terms as $term) : ?>
                    <li><a href="#" data-filter=".<?php echo $term->slug; ?>"><span><?php echo $term->name; ?></span></a></li>
                <?php endforeach; ?>
            </ul><!-- /portfolio-cats -->
        <?php } ?>	 
    </header><!-- /page-heading -->
    <section id="home-projects" class="clearfix">
        <article class="post full-width clearfix">
            <?php the_content(); ?>
        </article>
    </section>
    <?php if (sizeof($children)): ?>
        <style>
            h3 {
                margin-bottom: 13px;
                margin-top: 13px;
            }
            hr{
                border-style: solid;
                border-color: #0C741D;
            }
        </style>
        <hr/>
        <h3><?= the_title() ?></h3>
        <hr/>
        <section id="home-projects" class="clearfix">
            <?php foreach ($children as $item): ?>
                <?php $img = get_post_thumbnail_id($item->ID) ?>
                <?php preg_match_all('/http\:\/\/([^\"])+/', get_the_post_thumbnail($item->ID), $match); ?>
                <?php if ($img): ?>
                    <article class="portfolio-item col">
                        <a href="<?= get_page_link($item->ID) ?>" title="<?= $item->post_title ?>">
                            <img src="<?= $match[0][0] ?>">
                            <div class="portfolio-overlay">
                                <h3><?= $item->post_title ?></h3>
                            </div><!-- portfolio-overlay -->

                        </a>
                    </article>
                <?php endif ?>
            <?php endforeach; ?>
        </section>
    <?php endif; ?>

<?php endwhile; ?>
<?php get_footer() ?>